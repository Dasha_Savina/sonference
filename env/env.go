package env

import (
	"os"

)

var (
	ServiceHost = os.Getenv("SERVICE_HOST")
  FirebaseLogin = os.Getenv("FIREBASE_LOGIN")
  FirebasePassvord = os.Getenv("FIREBASE_PASSVORD")
	FirebaseAppKey = os.Getenv("FIREBASE_APP_KEY")

	JWTSecret = os.Getenv("JWT_SECRET")
	// Envs.
)
