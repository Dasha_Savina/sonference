package memory

import (
	"bitbucket.org/confa/back/api"
	"bitbucket.org/confa/back/model"
	"log"
)
var conferenceData = make(map[string]model.Conference)

// бд конференции
type ConferenceStorier struct {
}

// Создание новой бд конференции
func NewConferenceStorier() (s ConferenceStorier, e error) {
	return
}

func (s ConferenceStorier) Create(value *model.Conference) error {
	conferenceData[value.UID] = *value
	return nil
}

func (s ConferenceStorier) List(match interface{}) ([]model.Conference, error) {
	var ri []model.Conference
	for _, v := range conferenceData {
		ri = append(ri, v)
	}
	return ri, nil
}

func (s ConferenceStorier) Get(key string) (*model.Conference, error) {
	for k, v := range conferenceData {
		if key == k {
			return &v, nil
		}
	}
	data, success := conferenceData[key]
	if !success {
		return nil, api.ErrNotFound
	}

	return &data, nil
}

func (s ConferenceStorier) Put(value *model.Conference) error {
  	for k, _ := range conferenceData {
  		if k == value.UID {
  			conferenceData[k] = *value
  			return nil
  		}
  	}
  	return api.ErrNotFound
  }

func (s ConferenceStorier) Delete(key string) error {
  	for k, _ := range conferenceData {
  		log.Println("k: ", k, ", key: ", key)
  		if k == key {
  			delete(conferenceData, key)
  			return nil
  		}
  	}
  	return nil
  }
