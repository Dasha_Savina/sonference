package memory

// Структура хранение бд
type MemoryStorier struct {
	BookStorier
}

// Создание новой бд
func NewStorier() (s MemoryStorier, e error) {
	bs, err := NewBookStorier()
	if err != nil {
		return s, err
	}

	s.BookStorier = bs
	return
}
