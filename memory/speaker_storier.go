package memory


import (
	"bitbucket.org/confa/back/api"
	"bitbucket.org/confa/back/model"
)

var speakerData = make(map[string]map[string]model.Speaker)

type SpeakerStorier struct {
}

func NewSpeakerStorier() (s SpeakerStorier, e error) {
	return
}

func (s SpeakerStorier) Create(conf string, value *model.Speaker) error {
	_, success := speakerData[conf]
	if !success {
		speakerData[conf] = make(map[string]model.Speaker)
	}


	speakerData[conf][value.UID] = *value
	return nil
}

func (s SpeakerStorier) List(conf string) ([]model.Speaker, error) {
	var ri []model.Speaker
	for _, v := range speakerData[conf] {
		ri = append(ri, v)
	}
	return ri, nil
}

func (s SpeakerStorier) Get(conf string, key string) (*model.Speaker, error) {
	for k, v := range speakerData[conf] {
		if key == k {
			return &v, nil
		}
	}

	data, success := speakerData[conf][key]
	if !success {
		return nil, api.ErrNotFound
	}

	return &data, nil
}

func (s SpeakerStorier) Put(conf string, value *model.Speaker) error {
	for k, _ := range speakerData[conf] {
		if k == value.UID {
			speakerData[conf][k] = *value
			return nil
		}
	}
	return api.ErrNotFound
}

func (s SpeakerStorier) Delete(conf string, key string) error {
	for k, _ := range speakerData[conf] {
		if k == key {
			data, success := speakerData[conf]
			if success {
				delete(data, key)
			}
			return nil
		}
	}
	return nil
}
