package memory

import (
	"log"

	"bitbucket.org/confa/back/api"
	"bitbucket.org/confa/back/model"
	"gopkg.in/mgo.v2/bson"
)

var roomData = make(map[bson.ObjectId]model.Book)

// бд книги
type BookStorier struct {
}

// Создание новой бд книги
func NewBookStorier() (s BookStorier, e error) {
	return
}

// Создание
func (s BookStorier) Create(value *model.Book) error {
	id := bson.NewObjectId()
	value.ID = id
	roomData[id] = *value
	return nil
}

// Список
func (s BookStorier) List(match interface{}, skip, limit int) ([]model.Book, error) {
	var ri []model.Book
	for _, v := range roomData {
		ri = append(ri, v)
	}
	return ri, nil
}

// Получение
func (s BookStorier) Get(key string) (*model.Book, error) {
	for k, v := range roomData {
		if key == k.Hex() {
			return &v, nil
		}
	}

	data, success := roomData[bson.ObjectIdHex(key)]
	if !success {
		return nil, api.ErrNotFound
	}

	return &data, nil
}

// Обновление
func (s BookStorier) Put(value *model.Book) error {
	for k, _ := range roomData {
		if k == value.ID {
			roomData[k] = *value
			return nil
		}
	}
	return api.ErrNotFound
}

// Удаление
func (s BookStorier) Delete(key string) error {
	for k, _ := range roomData {
		log.Println("k: ", k, ", key: ", key)
		if k.Hex() == key {
			delete(roomData, bson.ObjectIdHex(key))
			return nil
		}
	}
	return nil
}
