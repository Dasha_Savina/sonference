package memory


import (
	"bitbucket.org/confa/back/api"
	"bitbucket.org/confa/back/model"
)

var ratingData = make(map[string]map[string]map[string]model.Rating)

// бд рейтинга
type RatingStorier struct {
}

// Создание новой бд рейтинга
func NewRatingStorier() (s RatingStorier, e error) {
	return
}

func (s RatingStorier) Create(conf string, eventUID string, value *model.Rating) error {
	_, success := ratingData[conf]
	if !success {
		ratingData[conf] = make(map[string]map[string]model.Rating)
	}

	_, success = ratingData[conf][eventUID]
	if !success {
		ratingData[conf][eventUID] = make(map[string]model.Rating)
	}

	ratingData[conf][eventUID][value.UID] = *value
	return nil
}

func (s RatingStorier) List(conf string, eventUID string) ([]model.Rating, error) {
	var ri []model.Rating
	for _, v := range ratingData[conf][eventUID] {
		ri = append(ri, v)
	}
	return ri, nil
}

func (s RatingStorier) Get(conf string, eventUID string, key string) (*model.Rating, error) {
	for k, v := range ratingData[conf][eventUID] {
		if key == k {
			return &v, nil
		}
	}

	data, success := ratingData[conf][eventUID][key]
	if !success {
		return nil, api.ErrNotFound
	}

	return &data, nil
}

func (s RatingStorier) Put(conf string, eventUID string, value *model.Rating) error {
	for k, _ := range ratingData[conf][eventUID] {
		if k == value.UID {
			ratingData[conf][eventUID][k] = *value
			return nil
		}
	}
	return api.ErrNotFound
}

func (s RatingStorier) Delete(conf string, eventUID string, key string) error {
	for k, _ := range ratingData[conf][eventUID] {
		if k == key {
			data, success := ratingData[conf][eventUID]
			if success {
				delete(data, key)
			}
			return nil
		}
	}
	return nil
}
