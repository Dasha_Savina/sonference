package memory


import (
	"bitbucket.org/confa/back/api"
	"bitbucket.org/confa/back/model"
)

var trackData = make(map[string]map[string]model.Track)

type TrackStorier struct {
}

func NewTrackStorier() (s TrackStorier, e error) {
	return TrackStorier{}, nil
}

func (s TrackStorier) Create(conf string, value *model.Track) error {
	_, success := trackData[conf]
	if !success {
		trackData[conf] = make(map[string]model.Track)
	}


	trackData[conf][value.UID] = *value
	return nil
}

func (s TrackStorier) List(conf string) ([]model.Track, error) {
	var ri []model.Track

	if _, success := trackData[conf]; !success {
		return ri, api.ErrNotFound
	}

	for _, v := range trackData[conf] {
		ri = append(ri, v)
	}
	return ri, nil
}

func (s TrackStorier) Get(conf string, key string) (*model.Track, error) {
	if _, success := trackData[conf]; !success {
		return nil, api.ErrNotFound
	}

	data, success := trackData[conf][key]
	if !success {
		return nil, api.ErrNotFound
	}

	return &data, nil
}

func (s TrackStorier) Put(conf string, value *model.Track) error {
	for k, _ := range trackData[conf] {
		if k == value.UID {
			trackData[conf][k] = *value
			return nil
		}
	}
	return api.ErrNotFound
}

func (s TrackStorier) Delete(conf string, key string) error {
	for k, _ := range trackData[conf] {
		if k == key {
			data, success := trackData[conf]
			if success {
				delete(data, key)
			}
			return nil
		}
	}
	return nil
}
