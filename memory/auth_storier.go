package memory

import (
	"bitbucket.org/confa/back/api"
  fauth "firebase.google.com/go/auth"
)

var authData = make(map[string]fauth.Token)

type AuthStorier struct {
}

func NewAuthStorier() (s AuthStorier, e error) {
	return AuthStorier{}, nil
}

func (s AuthStorier) Put(token string, value *fauth.Token) error {
  authData[token] = *value
	return nil
}

func (s AuthStorier) Get(token string) (*fauth.Token, error) {
	data, success := authData[token]
	if !success {
		return nil, api.ErrNotFound
	}

	return &data, nil
}

func (s AuthStorier) Delete(token string) error {
	for k, _ := range authData {
		if k == token {
			delete(authData, token)
			return nil
		}
	}
	return nil
}
