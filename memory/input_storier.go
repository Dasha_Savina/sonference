package memory

import (
	"bitbucket.org/confa/back/api"
  fauth "firebase.google.com/go/auth"
)

var inputData = make(map[string]fauth.Token)

type InputStorier struct {
}

func NewInputStorier() (s InputStorier, e error) {
	return InputStorier{}, nil
}

func (s InputStorier) Put(token string, value *fauth.Token) error {
  inputData[token] = *value
	return nil
}

func (s InputStorier) Get(token string) (*fauth.Token, error) {
	data, success := inputData[token]
	if !success {
		return nil, api.ErrNotFound
	}

	return &data, nil
}

func (s InputStorier) Delete(token string) error {
	for k, _ := range inputData {
		if k == token {
			delete(inputData, token)
			return nil
		}
	}
	return nil
}
