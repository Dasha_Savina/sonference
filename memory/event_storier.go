package memory


import (
	"bitbucket.org/confa/back/api"
	"bitbucket.org/confa/back/model"
)

var eventData = make(map[string]map[string]model.Event)

type EventStorier struct {
}

func NewEventStorier() (s EventStorier, e error) {
	return EventStorier{}, nil
}

func (s EventStorier) Create(conf string, value *model.Event) error {
	_, success := eventData[conf]
	if !success {
		eventData[conf] = make(map[string]model.Event)
	}


	eventData[conf][value.UID] = *value
	return nil
}

func (s EventStorier) List(conf string) ([]model.Event, error) {
	var ri []model.Event
	for _, v := range eventData[conf] {
		ri = append(ri, v)
	}
	return ri, nil
}

func (s EventStorier) Get(conf string, key string) (*model.Event, error) {
	for k, v := range eventData[conf] {
		if key == k {
			return &v, nil
		}
	}

	data, success := eventData[conf][key]
	if !success {
		return nil, api.ErrNotFound
	}

	return &data, nil
}

func (s EventStorier) Put(conf string, value *model.Event) error {
	for k, _ := range eventData[conf] {
		if k == value.UID {
			eventData[conf][k] = *value
			return nil
		}
	}
	return api.ErrNotFound
}

func (s EventStorier) Delete(conf string, key string) error {
	for k, _ := range eventData[conf] {
		if k == key {
			data, success := eventData[conf]
			if success {
				delete(data, key)
			}
			return nil
		}
	}
	return nil
}
