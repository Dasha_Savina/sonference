package memory


import (
	"bitbucket.org/confa/back/api"
	"bitbucket.org/confa/back/model"
)

var sectionData = make(map[string]map[string]model.Section)

type SectionStorier struct {
}

func NewSectionStorier() (s SectionStorier, e error) {
	return
}

func (s SectionStorier) Create(conf string, value *model.Section) error {
	_, success := sectionData[conf]
	if !success {
		sectionData[conf] = make(map[string]model.Section)
	}


	sectionData[conf][value.UID] = *value
	return nil
}

func (s SectionStorier) List(conf string) ([]model.Section, error) {
	var ri []model.Section
	for _, v := range sectionData[conf] {
		ri = append(ri, v)
	}
	return ri, nil
}

func (s SectionStorier) Get(conf string, key string) (*model.Section, error) {
	for k, v := range sectionData[conf] {
		if key == k {
			return &v, nil
		}
	}

	data, success := sectionData[conf][key]
	if !success {
		return nil, api.ErrNotFound
	}

	return &data, nil
}

func (s SectionStorier) Put(conf string, value *model.Section) error {
	for k, _ := range sectionData[conf] {
		if k == value.UID {
			sectionData[conf][k] = *value
			return nil
		}
	}
	return api.ErrNotFound
}

func (s SectionStorier) Delete(conf string, key string) error {
	for k, _ := range sectionData[conf] {
		if k == key {
			data, success := sectionData[conf]
			if success {
				delete(data, key)
			}
			return nil
		}
	}
	return nil
}
