package service

import (
  "net/http"

  "github.com/gin-gonic/gin"
  "github.com/Sirupsen/logrus"
  "bitbucket.org/confa/back/api"
  "bitbucket.org/confa/back/model"
  "bitbucket.org/confa/back/firebase"
  "bitbucket.org/confa/back/utils"
)

const (
  ParamSpeakerUID = "id"
)

// Спикер
type Speaker struct {
  log *logrus.Logger
  db  SpeakerStorier
  firebase *firebase.Firebase
}

// Создание нового спикера
func NewSpeaker(log *logrus.Logger, db SpeakerStorier, firebase *firebase.Firebase) Speaker {
	return Speaker{log, db, firebase}
}

// Загрузка спикеров
func (s *Speaker) LoadSpeakers() error {
//  list, err := firebase
return nil
}

func (s *Speaker) ListHandler(c *gin.Context) {
  confr, _ := c.Get(ParamConfID)
  conf := confr.(string)

	response := api.BaseResponse{}

	result, err := s.db.List(conf)
	if err != nil {
		s.log.Error(err)
	}
  if result == nil {
    result = []model.Speaker{}
  }

	response.Data = result
	c.JSON(http.StatusOK, response)
}

// Получение спикеров
func (s *Speaker) GetHandler(c *gin.Context) {
	uid := c.Param(ParamSpeakerUID)
  confr, _ := c.Get(ParamConfID)
  conf := confr.(string)

	response := api.BaseResponse{}

	result, err := s.db.Get(conf, uid)
	if err != nil {
  	s.log.Error(err)
    response.Error = api.ErrWriteDbSpeaker
    c.JSON(http.StatusOK, response)
    return
	}

	response.Data = result
	c.JSON(http.StatusOK, response)
}

//Создание спикеров
func (s *Speaker) CreateHandler(c *gin.Context) {
  confr, _ := c.Get(ParamConfID)
  conf := confr.(string)

	response := api.BaseResponse{}

  cs := api.CreateSpeaker{}
	err := c.BindJSON(&cs)
	if err != nil {
    s.log.Error(err)
		response.Error = api.ErrBindCreateSpeaker
		c.JSON(http.StatusBadRequest, response)
		return
	}

	model := &model.Speaker{
    UID: utils.RandString(16),
		Name: cs.Name,
    Work: cs.Work,
    Info: cs.Info,
    Photo: cs.Photo,
	}

  err = s.firebase.WriteSpeaker(conf, model)
  if err != nil {
    s.log.Error(err)
		response.Error = api.ErrWriteFirebase
		c.JSON(http.StatusBadRequest, response)
		return
	}

	err = s.db.Create(conf, model)
	if err != nil {
		response.Error = api.ErrWriteDbSpeaker
		c.JSON(http.StatusBadRequest, response)
		return
	}

  response.Data = model

	c.JSON(http.StatusCreated, response)
}

//Обновление спикеров
func (s *Speaker) UpdateHandler(c *gin.Context) {
  confr, _ := c.Get(ParamConfID)
  conf := confr.(string)
  id := c.Param(ParamSpeakerUID)

	response := api.BaseResponse{}

	cs := api.CreateSpeaker{}
	err := c.BindJSON(&cs)
	if err != nil {
    s.log.Error(err)
		response.Error = api.ErrBindCreateSpeaker
		c.JSON(http.StatusBadRequest, response)
		return
	}

  model := &model.Speaker{
    UID: id,
		Name: cs.Name,
    Work: cs.Work,
    Info: cs.Info,
    Photo: cs.Photo,
	}

  err = s.firebase.WriteSpeaker(conf, model)
  if err != nil {
    s.log.Error(err)
		response.Error = api.ErrWriteFirebase
		c.JSON(http.StatusBadRequest, response)
		return
	}

	err = s.db.Put(conf, model)
	if err != nil {
		response.Error = api.ErrWriteDbSpeaker
		c.JSON(http.StatusBadRequest, response)
		return
	}

	c.Status(http.StatusOK)
}

//Удаление спикеров
func (s *Speaker) DeleteHandler(c *gin.Context) {
  confr, _ := c.Get(ParamConfID)
  conf := confr.(string)
  uid := c.Param(ParamSpeakerUID)

	response := api.BaseResponse{}

  err := s.firebase.DeleteSpeaker(conf, uid)
  if err != nil {
    s.log.Error(err)
		response.Error = api.ErrWriteFirebase
		c.JSON(http.StatusBadRequest, response)
		return
	}

	err = s.db.Delete(conf, uid)
	if err != nil {
		response.Error = api.ErrWriteDbSpeaker
		c.JSON(http.StatusBadRequest, response)
		return
	}

	c.Status(http.StatusOK)
}

func (s *Speaker) Route(r *gin.Engine, middl gin.HandlerFunc) {
  g := r.Group("speakers")
  g.Use(middl)
  {
    g.GET("/", s.ListHandler)
    g.GET("/:id", s.GetHandler)
    g.POST("/", s.CreateHandler)
    g.PUT("/:id", s.UpdateHandler)
    g.DELETE("/:id", s.DeleteHandler)
  }
}
