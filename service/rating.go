package service

import (
  "net/http"

  "github.com/gin-gonic/gin"
  "github.com/Sirupsen/logrus"

  "bitbucket.org/confa/back/api"
  "bitbucket.org/confa/back/model"
  "bitbucket.org/confa/back/firebase"
  "bitbucket.org/confa/back/utils"
)

const (
  ParamRatingUID = "id"
)

// Рейтинг
type Rating struct {
  log *logrus.Logger
  db  RatingStorier
  firebase *firebase.Firebase
}

// Создание нового рейтинга
func NewRating(log *logrus.Logger, db RatingStorier, firebase *firebase.Firebase) Rating {
	return Rating{log, db, firebase}
}

// Загрузка Рейтинга
func (s *Rating) LoadRating() error {
//  list, err := firebase
return nil
}

// Список рейтинга
func (s *Rating) ListHandler(c *gin.Context) {
  confr, _ := c.Get(ParamConfID)
  conf := confr.(string)
  eventUID := c.Param(ParamEventUID)

  response := api.BaseResponse{}

  result, err := s.db.List(conf, eventUID)
  if err != nil {
    s.log.Error(err)
  }
  if result == nil {
    result = []model.Rating{}
  }

  response.Data = result
  c.JSON(http.StatusOK, response)
  }

// Получение рейтинга
func (s *Rating) GetHandler(c *gin.Context) {
  uid := c.Param(ParamRatingUID)
  confr, _ := c.Get(ParamConfID)
  conf := confr.(string)
  eventUID := c.Param(ParamEventUID)

	response := api.BaseResponse{}

	result, err := s.db.Get(conf, eventUID, uid)
	if err != nil {
  	s.log.Error(err)
    response.Error = err
    c.JSON(http.StatusOK, response)
    return
	}

	response.Data = result
	c.JSON(http.StatusOK, response)
}

// Создание рейтинга
func (s *Rating) CreateHandler(c *gin.Context) {
  confr, _ := c.Get(ParamConfID)
  conf := confr.(string)
  eventUID := c.Param(ParamEventUID)

  response := api.BaseResponse{}

  cs := api.CreateRating{}
  err := c.BindJSON(&cs)
  if err != nil {
    s.log.Error(err)
    response.Error = api.ErrBindCreateRating
    c.JSON(http.StatusBadRequest, response)
    return
  }

	model := &model.Rating{
    UID: utils.RandString(16),
		Rating: cs.Rating,
    Comment: cs.Comment,
	}

  err = s.firebase.WriteRating(conf, eventUID, model)
  if err != nil {
    s.log.Error(err)
    response.Error = api.ErrWriteFirebase
    c.JSON(http.StatusBadRequest, response)
    return
  }

  err = s.db.Create(conf, eventUID, model)
  if err != nil {
    response.Error = api.ErrWriteDbRating
    c.JSON(http.StatusBadRequest, response)
    return
  }

  response.Data = model

  c.JSON(http.StatusCreated, response)
}

// Обновление рейтинга
func (s *Rating) UpdateHandler(c *gin.Context) {
  confr, _ := c.Get(ParamConfID)
  conf := confr.(string)
  eventUID := c.Param(ParamEventUID)
  id := c.Param(ParamRatingUID)

  response := api.BaseResponse{}

  cs := api.CreateRating{}
  err := c.BindJSON(&cs)
  if err != nil {
    s.log.Error(err)
    response.Error = api.ErrBindCreateRating
    c.JSON(http.StatusBadRequest, response)
    return
  }

  model := &model.Rating{
    UID: id,
		Rating: cs.Rating,
    Comment: cs.Comment,
	}

  err = s.firebase.WriteRating(conf, eventUID, model)
  if err != nil {
    s.log.Error(err)
    response.Error = api.ErrWriteFirebase
    c.JSON(http.StatusBadRequest, response)
    return
  }

  err = s.db.Put(conf, eventUID, model)
  if err != nil {
    response.Error = api.ErrWriteDbRating
    c.JSON(http.StatusBadRequest, response)
    return
  }

  c.Status(http.StatusOK)
}
// Удаление рейтинга
func (s *Rating) DeleteHandler(c *gin.Context) {
  confr, _ := c.Get(ParamConfID)
  conf := confr.(string)
  eventUID := c.Param(ParamEventUID)
  uid := c.Param(ParamRatingUID)

  response := api.BaseResponse{}

  err := s.firebase.DeleteRating(conf, eventUID, uid)
  if err != nil {
    s.log.Error(err)
    response.Error = api.ErrWriteFirebase
    c.JSON(http.StatusBadRequest, response)
    return
  }

  err = s.db.Delete(conf, eventUID, uid)
  if err != nil {
    response.Error = api.ErrWriteDbRating
    c.JSON(http.StatusBadRequest, response)
    return
  }

  c.Status(http.StatusOK)
  }

func (s *Rating) Route(r *gin.Engine, middl gin.HandlerFunc) {
  g := r.Group("ratings")
  g.Use(middl)
  {
    g.GET("/:event_uid/", s.ListHandler)
    g.GET("/:event_uid/:id", s.GetHandler)
    g.POST("/:event_uid/", s.CreateHandler)
    g.PUT("/:event_uid/:id", s.UpdateHandler)
    g.DELETE("/:event_uid/:id", s.DeleteHandler)
  }
}
