package service

import (
	"net/http"

	"bitbucket.org/confa/back/api"
	"bitbucket.org/confa/back/model"
	"github.com/Sirupsen/logrus"
	"github.com/gin-gonic/gin"
	"gopkg.in/mgo.v2/bson"
)

const (
	QuerySkip  = "skip"
	QueryLimit = "limit"

	ParamBookID = "id"
)

// Книга
type Book struct {
	log *logrus.Logger
	db  Storier
}

// Создание новой книги
func NewBook(log *logrus.Logger, db Storier) Book {
	return Book{log, db}
}

// Список обработанных книг
func (b *Book) ListHandler(c *gin.Context) {
	sp := c.Query(QuerySkip)
	lt := c.Query(QueryLimit)

	request := api.NewListRequest(sp, lt)

	response := api.BaseResponse{}

	result, err := b.db.List(bson.M{}, request.Skip, request.Limit)
	if err != nil {
		b.log.Error(err)
	}
	if result == nil {
		result = []model.Book{}
	}
	response.Data = result
	c.JSON(http.StatusOK, response)
}

// Получение обработанных книг
func (b *Book) GetHandler(c *gin.Context) {
	id := c.Param(ParamBookID)
	request := api.NewGetRequest(id)

	response := api.BaseResponse{}

	result, err := b.db.Get(request.BookID)
	if err != nil {
		b.log.Error(err)
	}
	response.Data = result
	c.JSON(http.StatusOK, response)
}
// Создание обработанных книг
func (b *Book) CreateHandler(c *gin.Context) {
	response := api.BaseResponse{}

	book := api.CreateBook{}
	err := c.BindJSON(&book)
	if err != nil {
		response.Error = api.ErrBindCreateBook
		c.JSON(http.StatusBadRequest, response)
		return
	}
	model := &model.Book{
		Name: book.Name,
	}
	err = b.db.Create(model)
	if err != nil {
		response.Error = api.ErrBindCreateBook
		c.JSON(http.StatusBadRequest, response)
		return
	}
	c.Status(http.StatusCreated)
}

// Обновление обработанных книг
func (b *Book) UpdateHandler(c *gin.Context) {
	response := api.BaseResponse{}

	book := api.UpdateBook{}
	err := c.BindJSON(&book)
	if err != nil {
		response.Error = api.ErrWriteDbBook
		c.JSON(http.StatusBadRequest, response)
		return
	}
	model := &model.Book{
		ID:   bson.ObjectIdHex(book.ID),
		Name: book.Name,
	}
	err = b.db.Put(model)
	if err != nil {
		response.Error = api.ErrWriteDbBook
		c.JSON(http.StatusBadRequest, response)
		return
	}
	c.Status(http.StatusCreated)
}

// Удаление обработанных книг
func (b *Book) DeleteHandler(c *gin.Context) {
	id := c.Param(ParamBookID)

	response := api.BaseResponse{}

	request := api.NewGetRequest(id)

	err := b.db.Delete(request.BookID)
	if err != nil {
		response.Error = api.ErrWriteDbBook
		c.JSON(http.StatusBadRequest, response)
		return
	}
	c.Status(http.StatusOK)
}

func (b *Book) Route(route *gin.Engine) {
	g := route.Group("books")
	{
		g.GET("/", b.ListHandler)
		g.GET("/:id", b.GetHandler)
		g.POST("/", b.CreateHandler)
		g.PUT("/", b.UpdateHandler)
		g.DELETE("/:id", b.DeleteHandler)
	}
}
