package service

import (
  "net/http"
  "encoding/base64"

  "github.com/Sirupsen/logrus"

  "bitbucket.org/confa/back/firebase"
  "bitbucket.org/confa/back/api"
  "bitbucket.org/confa/back/token"
  "github.com/gin-gonic/gin"
)

//
type Auth struct {
  log *logrus.Logger
  firebase *firebase.Firebase
  db  StorierAuth
  tm *token.JWT
}

// Создание новой конференции
func NewAuth(log *logrus.Logger, firebase *firebase.Firebase, db StorierAuth, tm *token.JWT) Auth {
	return Auth{log, firebase, db, tm}
}

func (a *Auth) SignIn(c *gin.Context) {
  response := api.BaseResponse{}

  r := api.SignIn{}
  err := c.BindJSON(&r)
  if err != nil {
    a.log.Error(err)
    response.Error = api.ErrBindSignIn
    c.JSON(http.StatusBadRequest, response)
    return
  }

  user, err := a.firebase.GetUserByEmail(r.Login)
  ph := base64.RawURLEncoding.EncodeToString([]byte(r.Password))
  if user.CustomClaims["pass_hash"].(string) != ph {
    response.Error = api.ErrBindSignIn// Invalid pasword
    c.JSON(http.StatusBadRequest, response)
    return
  }

  token, err := a.tm.Generate(user.UID, token.RoleUser)
  if err != nil {
    a.log.Error(err)
    response.Error = api.ErrBindSignIn
    c.JSON(http.StatusBadRequest, response)
    return
  }

  response.Data = token
  c.JSON(http.StatusOK, response)
}

func (a *Auth) Logout(c *gin.Context) {
  response := api.BaseResponse{}

  uid, _  := c.Get("user_uid")

  err := a.firebase.RevokeToken(uid.(string))

  if err != nil {
    a.log.Error(err)
    response.Error = api.ErrNotFound
    c.JSON(http.StatusBadRequest, response)
    return
  }

  c.Status(http.StatusOK)
}

func (a *Auth) Route(r *gin.Engine) {
  g := r.Group("auth")
  {
    g.POST("/access", a.SignIn)
    g.POST("/revoke", a.Logout)
  }
}
