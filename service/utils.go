package service

import (
  "bitbucket.org/confa/back/model"
  "bitbucket.org/confa/back/firebase"
)

func SyncFirebaseCache(firebase *firebase.Firebase,
                      sconf StorierConference,
                      ssect SectionStorier,
                      sspec SpeakerStorier,
                      strac TrackStorier,
                      seven EventStorier,
                      sratg RatingStorier) error {

  // Загрузка списка конференций.
  conf, err := firebase.ListUsers()
  if err != nil {
    //TODO return err
  }

  confIds := make([]string, len(conf))
  for _, u := range conf {
    c := model.Conference{u.UID, u.DisplayName, u.Email}
    err = sconf.Create(&c)
    if err != nil {
      return err
    }
    confIds = append(confIds, c.UID)
  }

  // секций

  for _, id := range confIds {
    items, err := firebase.ListSection(id)
    if err != nil {
      return err
    }
    for _, section := range items {
      err := ssect.Create(id, &section)
      if err != nil {
        return err
      }
    }
  }

  //Спкеров

  for _, id := range confIds {
    items, err := firebase.ListSpeaker(id)
    if err != nil {
      return err
    }
    for _, speaker := range items {
      err := sspec.Create(id, &speaker)
      if err != nil {
        return err
      }
    }
  }

  // Треков

  for _, id := range confIds {
    items, err := firebase.ListTrack(id)
    if err != nil {
      return err
    }
    for _, track := range items {
      err := strac.Create(id, &track)
      if err != nil {
        return err
      }
    }
  }

  // События

  for _, id := range confIds {
    items, err := firebase.ListEvent(id)
    if err != nil {
      return err
    }
    for _, event := range items {
      err := seven.Create(id, &event)
      if err != nil {
        return err
      }

      // Рейтингов

      items, err := firebase.ListRating(id, event.UID)
      if err != nil {
        return err
      }
      for _, rating := range items {
        err := sratg.Create(id, event.UID, &rating)
        if err != nil {
          return err
        }
      }
    }
  }

  return nil
}
