package service

import (
	"bitbucket.org/confa/back/model"
	fauth "firebase.google.com/go/auth"
)

// Storier book.
type Storier interface {
	Create(value *model.Book) error
	// List books.
	List(match interface{}, skip, limit int) ([]model.Book, error)
	// Get book.
	Get(key string) (*model.Book, error)
	// Put book.
	Put(value *model.Book) error
	// Delete book
	Delete(key string) error
}

type StorierAuth interface {
	Put(token string, value *fauth.Token) error
	Get(token string) (*fauth.Token, error)
	Delete(token string) error
}

 type StorierConference interface{
	 Create(value *model.Conference) error
	 // список конференций
	 // match - условие
	 List(match interface{})([]model.Conference, error)
	 // получить конф
	 	Get(key string) (*model.Conference, error)
		//
		Put(value *model.Conference) error
		// Delete book
		Delete(key string) error
 }

// Секции.
 type SectionStorier interface {
	 // Создание
	 Create(conf string, value *model.Section) error
	 // Список
	 List(conf string)([]model.Section, error)
	 // Получить
	 Get(conf string, key string) (*model.Section, error)
	 // Обновить
	 Put(conf string, value *model.Section) error
	 // Удалить
	 Delete(conf string, key string) error
}

// Спикеры.
 type SpeakerStorier interface {
	 // Создание
	 Create(conf string, value *model.Speaker) error
	 // Список
	 List(conf string)([]model.Speaker, error)
	 // Получить
	 Get(conf string, key string) (*model.Speaker, error)
	 // Обновить
	 Put(conf string, value *model.Speaker) error
	 // Удалить
	 Delete(conf string, key string) error
}

// Треки.
 type TrackStorier interface {
	 // Создание
	 Create(conf string, value *model.Track) error
	 // Список
	 List(conf string)([]model.Track, error)
	 // Получить
	 Get(conf string, key string) (*model.Track, error)
	 // Обновить
	 Put(conf string, value *model.Track) error
	 // Удалить
	 Delete(conf string, key string) error
}

// Событие.
 type EventStorier interface {
	 // Создание
	 Create(conf string, value *model.Event) error
	 // Список
	 List(conf string)([]model.Event, error)
	 // Получить
	 Get(conf string, key string) (*model.Event, error)
	 // Обновить
	 Put(conf string, value *model.Event) error
	 // Удалить
	 Delete(conf string, key string) error
}

// Рейтинг.
 type RatingStorier interface {
	 // Создание
	 Create(conf string, eventUID string, value *model.Rating) error
	 // Список
	 List(conf string, eventUID string)([]model.Rating, error)
	 // Получить
	 Get(conf string, eventUID string, key string) (*model.Rating, error)
	 // Обновить
	 Put(conf string, eventUID string, value *model.Rating) error
	 // Удалить
	 Delete(conf string, eventUID string, key string) error
}
