package service

import (
	"bitbucket.org/confa/back/model"
	"bitbucket.org/confa/back/firebase"
	"bitbucket.org/confa/back/utils"
)

// Тестирование Аутентификации
func (s *AppService) TestAuth(firebaseClient *firebase.Firebase) {
	s.log.Println("Create user")
	user, err := firebaseClient.CreateUser("bbubu@cfcf.vgh", "qwertyu0", "westewswet")
	if err != nil {
		s.log.Error(err)
		panic("Cannot create user.")
	}
	s.log.Println("Create user: ", user.UID)

	s.log.Println("Get user")
	user, err = firebaseClient.GetUser(user.UID)
	if err != nil {
		s.log.Error(err)
		panic("Cannot get user.")
	}
	s.log.Println("Get user UID: ", user.UID)
	s.log.Println("Get user Name: ", user.DisplayName)

	s.log.Println("Update user")
	user, err = firebaseClient.UpdateUser(user.UID, "asdasd@asda.asd", "passs11", "ameeeee")
	if err != nil {
		s.log.Error(err)
		panic("Cannot update user.")
	}
	s.log.Println("Get user UID: ", user.UID)
	s.log.Println("Get user Name: ", user.DisplayName)

	s.log.Println("Delete user")
	err = firebaseClient.DeleteUser(user.UID)
	if err != nil {
		s.log.Error(err)
		panic("Cannot delete user.")
	}
}

//Тестирование Секции
func (s *AppService) TestWriteSection(firebaseClient *firebase.Firebase) {
	s.log.Println("Get user")
	user, err := firebaseClient.GetUser("q1bLfL1Ip8cHRk1q8yxAfphZSuR2")
	if err != nil {
		s.log.Error(err)
		panic("Cannot get user.")
	}
	s.log.Println("Get user UID: ", user.UID)

	wsection := &model.Section {
		UID: utils.RandString(16),
		Name: "sec name",
		Color: "#aa3455",
	}
	err = firebaseClient.WriteSection(user.UID, wsection)
	if err != nil {
		s.log.Error(err)
		panic("Cannot write section.")
	}
	s.log.Println("WriteSction succes")
	rsection, err := firebaseClient.ReadSection(user.UID, wsection.UID)
	if err != nil {
		s.log.Error(err)
		panic("Cannot read section.")
	}
	s.log.Println("Read section UID: ", rsection.UID)
	list, err := firebaseClient.ListSection(user.UID)
	if err != nil {
		s.log.Error(err)
		panic("Cannot read list.")
	}
	s.log.Println("Read section list: ", len(list))
	err = firebaseClient.DeleteSection(user.UID, rsection.UID)
	if err != nil {
		s.log.Error(err)
		panic("Cannot delete section.")
	}
	s.log.Println("Delete section: ", rsection.UID)
}

// Тестировнаие спикера
func (s *AppService) TestWriteSpeaker(firebaseClient *firebase.Firebase) {
	s.log.Println("Get user")
	user, err := firebaseClient.GetUser("q1bLfL1Ip8cHRk1q8yxAfphZSuR2")
	if err != nil {
		s.log.Error(err)
		panic("Cannot get user.")
	}
	s.log.Println("Get user UID: ", user.UID)

	wspeaker := &model.Speaker {
		UID: utils.RandString(16),
		Name: "name speak",
		Work: "hhhh",
		Info: "gggg",
		Photo: "hcdsjr",
	}
	err = firebaseClient.WriteSpeaker(user.UID, wspeaker)
	if err != nil {
		s.log.Error(err)
		panic("Cannot write speaker.")
	}
	s.log.Println("WriteSpeaker succes")
	rspeaker, err := firebaseClient.ReadSpeaker(user.UID, wspeaker.UID)
	if err != nil {
		s.log.Error(err)
		panic("Cannot read speaker.")
	}
	s.log.Println("Read speaker UID: ", rspeaker.UID)
	list, err := firebaseClient.ListSpeaker(user.UID)
	if err != nil {
		s.log.Error(err)
		panic("Cannot read list.")
	}
	s.log.Println("Read speaker list: ", len(list))
	err = firebaseClient.DeleteSpeaker(user.UID, rspeaker.UID)
	if err != nil {
		s.log.Error(err)
		panic("Cannot delete speaker.")
	}
	s.log.Println("Delete speaker: ", rspeaker.UID)
}

// Тестирование трека
func (s *AppService) TestWriteTrack(firebaseClient *firebase.Firebase) {
	s.log.Println("Get user")
	user, err := firebaseClient.GetUser("q1bLfL1Ip8cHRk1q8yxAfphZSuR2")
	if err != nil {
		s.log.Error(err)
		panic("Cannot get user.")
	}
	s.log.Println("Get user UID: ", user.UID)

	wtrack := &model.Track {
		UID: utils.RandString(16),
		Name: "name track",
		Sort: "1",
	}

	err = firebaseClient.WriteTrack(user.UID, wtrack)
	if err != nil {
		s.log.Error(err)
		panic("Cannot write track.")
	}

	s.log.Println("WriteTrack succes")
	rtrack, err := firebaseClient.ReadTrack(user.UID, wtrack.UID)
	if err != nil {
		s.log.Error(err)
		panic("Cannot read track.")
	}

	s.log.Println("Read track UID: ", rtrack.UID)
	list, err := firebaseClient.ListTrack(user.UID)
	if err != nil {
		s.log.Error(err)
		panic("Cannot read list.")
	}

	s.log.Println("Read track list: ", len(list))
	err = firebaseClient.DeleteTrack(user.UID, rtrack.UID)
	if err != nil {
		s.log.Error(err)
		panic("Cannot delete track.")
	}

	s.log.Println("Delete track: ", rtrack.UID)
}

// Тестирование события
func (s *AppService) TestWriteEvent(firebaseClient *firebase.Firebase) {
	s.log.Println("Get user")
	user, err := firebaseClient.GetUser("q1bLfL1Ip8cHRk1q8yxAfphZSuR2")
	if err != nil {
		s.log.Error(err)
		panic("Cannot get user.")
	}
	s.log.Println("Get user UID: ", user.UID)

	wevent := &model.Event {
		UID: utils.RandString(16),
		TrackUID: utils.RandString(16),
		SectionUID: utils.RandString(16),
    Title: "Event ololo",
    Description: "My best event",
    StartTime: 0,
    Duration: 60,
    Audience: "123456",
    SpeakerUID: []string{
				utils.RandString(16),
				utils.RandString(16),
				utils.RandString(16),
		},
	}
	err = firebaseClient.WriteEvent(user.UID, wevent)
	if err != nil {
		s.log.Error(err)
		panic("Cannot write event.")
	}
	s.log.Println("WriteEvent succes")
	revent, err := firebaseClient.ReadEvent(user.UID, wevent.UID)
	if err != nil {
		s.log.Error(err)
		panic("Cannot read event.")
	}
	s.log.Println("Read event UID: ", revent.UID)
	list, err := firebaseClient.ListEvent(user.UID)
	if err != nil {
		s.log.Error(err)
		panic("Cannot read list.")
	}
	s.log.Println("Read event list: ", len(list))
	err = firebaseClient.DeleteEvent(user.UID, revent.UID)
	if err != nil {
		s.log.Error(err)
		panic("Cannot delete event.")
	}
	s.log.Println("Delete event: ", revent.UID)
}

// Тестирование полного события
func (s *AppService) TestWriteFullEvent(firebaseClient *firebase.Firebase) {
	s.log.Println("Get user")
	user, err := firebaseClient.GetUser("q1bLfL1Ip8cHRk1q8yxAfphZSuR2")
	if err != nil {
		s.log.Error(err)
		panic("Cannot get user.")
	}
	s.log.Println("Get user UID: ", user.UID)

	wfullevent := &model.FullEvent {
		UID: utils.RandString(16),
		Track: model.Track {
				UID: utils.RandString(16),
				Name: "yfytfyt",
				Sort: "12",
		},
		Section: model.Section{
			UID: utils.RandString(16),
			Name: "fcjk;l",
			Color: "ffc0cb",
		},
    Title: "Event ololo",
    Description: "My best event",
    StartTime: 0,
    Duration: 50,
    Audience: "654321",
    Speakers: []model.Speaker{
				model.Speaker{
					UID: utils.RandString(16),
					Name: "name speak",
					Work: "uuuu",
					Info: "gggg",
					Photo: "hhdauw",
				},
			},
	}
	err = firebaseClient.WriteFullEvent(user.UID, wfullevent)
	if err != nil {
		s.log.Error(err)
		panic("Cannot write fullevent.")
	}
	s.log.Println("WriteFullEvent succes")
	rfullevent, err := firebaseClient.ReadFullEvent(user.UID, wfullevent.UID)
	if err != nil {
		s.log.Error(err)
		panic("Cannot read fullevent.")
	}
	s.log.Println("Read fullevent UID: ", rfullevent.UID)
	list, err := firebaseClient.ListFullEvent(user.UID)
	if err != nil {
		s.log.Error(err)
		panic("Cannot read list.")
	}
	s.log.Println("Read fullevent list: ", len(list))
	err = firebaseClient.DeleteFullEvent(user.UID, rfullevent.UID)
	if err != nil {
		s.log.Error(err)
		panic("Cannot delete fullevent.")
	}
	s.log.Println("Delete fullevent: ", rfullevent.UID)
}

func (s *AppService) TestWriteRating(firebaseClient *firebase.Firebase) {
	s.log.Println("Get user")
	user, err := firebaseClient.GetUser("q1bLfL1Ip8cHRk1q8yxAfphZSuR2")
	if err != nil {
		s.log.Error(err)
		panic("Cannot get user.")
	}
	s.log.Println("Get user UID: ", user.UID)

	wrating := &model.Rating {
		UID: utils.RandString(16),
		Rating: 1,
		Comment: "hfhej",
	}
	eventUID := "eyexitfty_event_bhjjhb_"
	err = firebaseClient.WriteRating(user.UID, eventUID, wrating)
	if err != nil {
		s.log.Error(err)
		panic("Cannot write rating.")
	}
	s.log.Println("WriteRating succes")
	rrating, err := firebaseClient.ReadRating(user.UID, eventUID, wrating.UID)
	if err != nil {
		s.log.Error(err)
		panic("Cannot read rating.")
	}
	s.log.Println("Read rating UID: ", rrating.UID)
	list, err := firebaseClient.ListRating(user.UID, eventUID)
	if err != nil {
		s.log.Error(err)
		panic("Cannot read list.")
	}
	s.log.Println("Read rating list: ", len(list))
	//err = firebaseClient.DeleteRating(user.UID, rrating.UID)
	if err != nil {
		s.log.Error(err)
		panic("Cannot delete rating.")
	}
	s.log.Println("Delete rating: ", rrating.UID)
}
