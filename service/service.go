package service

import (
	"net/http"
	"time"

	"github.com/Sirupsen/logrus"
	"github.com/gin-gonic/contrib/ginrus"
	"github.com/gin-gonic/gin"

	"bitbucket.org/confa/back/env"
	"bitbucket.org/confa/back/memory"
	"bitbucket.org/confa/back/firebase"
	"bitbucket.org/confa/back/token"
	"bitbucket.org/confa/back/utils"
)

// Сервис
type AppService struct {
	Version string
	log     *logrus.Logger
}

// Создание нового сервиса
func NewAppService(version string, log *logrus.Logger) AppService {
	return AppService{version, log}
}

// Запуск сервиса.
func (s *AppService) Run() error {
	utils.SeedReset()
	firebaseClient, err := firebase.NewFirebase(s.log, env.FirebaseAppKey)
	if err != nil {
		panic("Cannot setup firebase.")
	}
	//
	sauth, err := memory.NewAuthStorier()
	if err != nil {
		panic("Cannot setup auth.")
	}
	// Установка конференции
	sconf, err := memory.NewConferenceStorier()
	if err != nil {
		panic("Cannot setup conference.")
	}
	// Установка секции
	ssect, err := memory.NewSectionStorier()
	if err != nil {
		panic("Cannot setup sections.")
	}
	// Установка спикеров
	sspec, err := memory.NewSpeakerStorier()
	if err != nil {
		panic("Cannot setup speakers.")
	}
	// Установка треков
	strac, err := memory.NewTrackStorier()
	if err != nil {
		panic("Cannot setup tracks.")
	}
	// Установка событий
	seven, err := memory.NewEventStorier()
	if err != nil {
		panic("Cannot setup events.")
	}
	// Установка рейтингов
	sratg, err := memory.NewRatingStorier()
	if err != nil {
		panic("Cannot setup ratings.")
	}

	tm := token.NewJWT(s.log, env.JWTSecret)
	auth := NewAuth(s.log, firebaseClient, sauth, &tm)
	conference := NewConference(s.log, firebaseClient, sconf)
	sections := NewSection(s.log, ssect, firebaseClient)
	speakers := NewSpeaker(s.log, sspec, firebaseClient)
	tracks := NewTrack(s.log, strac, firebaseClient)
	ratings := NewRating(s.log, sratg, firebaseClient)
	events := NewEvent(s.log, seven, firebaseClient)

	// Заполняем кеш
	err = SyncFirebaseCache(firebaseClient, sconf, ssect, sspec, strac, seven, sratg)
	if err != nil{
		s.log.Print(err)
		panic("Cannot load from firebase")
	}

/////

	r := gin.New()
	//a.Use(firebaseClient.Authentificate())
	r.Use(ginrus.Ginrus(s.log, time.RFC3339, true))

	auth.Route(r)
	conference.Route(r, tm.Authentificate(token.RoleAdmin))
	sections.Route(r, tm.Authentificate(token.RoleUser))
	speakers.Route(r, tm.Authentificate(token.RoleUser))
	tracks.Route(r, tm.Authentificate(token.RoleUser))
	ratings.Route(r, tm.Authentificate(token.RoleUser))
	events.Route(r, tm.Authentificate(token.RoleUser))
	r.GET("/ping", func(c *gin.Context) {
		c.String(http.StatusOK, "pong")
	})
	s.GenAdminToken(&tm)
	return r.Run(env.ServiceHost)
}

func (s *AppService) GenAdminToken(tc *token.JWT)  {
	 gtoken, err := tc.Generate("", token.RoleAdmin)
	 if err != nil {
	 	s.log.Error(err)
	 	panic("Cannot create token.")
	 }

	 s.log.Print("Admin token jwt: ", gtoken)
	 p, err := tc.Parse(gtoken)
	 if err != nil {
	 	s.log.Error(err)
	 	panic("Cannot parse token.")
	 }
	 s.log.Printf("%+v", p)
}
