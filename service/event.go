package service

import (
  "net/http"

  "github.com/gin-gonic/gin"
  "github.com/Sirupsen/logrus"

  "bitbucket.org/confa/back/api"
  "bitbucket.org/confa/back/model"
  "bitbucket.org/confa/back/firebase"
  "bitbucket.org/confa/back/utils"
)

const (
  ParamEventUID = "event_uid"
)

// События
type Event struct {
  log *logrus.Logger
  db  EventStorier
  firebase *firebase.Firebase
}

// Создание нового события
func NewEvent(log *logrus.Logger, db EventStorier, firebase *firebase.Firebase) Event {
	return Event{log, db, firebase}
}

// Загрузка события
func (s *Event) LoadEvent() error {
//  list, err := firebase
return nil
}

// Список событий
func (s *Event) ListHandler(c *gin.Context) {
  confr, _ := c.Get(ParamConfID)
  conf := confr.(string)

  response := api.BaseResponse{}

  result, err := s.db.List(conf)
  if err != nil {
    s.log.Error(err)
  }
  if result == nil {
    result = []model.Event{}
  }

  response.Data = result
  c.JSON(http.StatusOK, response)
  }

// Получение события
func (s *Event) GetHandler(c *gin.Context) {
  	uid := c.Param(ParamEventUID)
    confr, _ := c.Get(ParamConfID)
    conf := confr.(string)

  	response := api.BaseResponse{}

  	result, err := s.db.Get(conf, uid)
  	if err != nil {
    	s.log.Error(err)
      response.Error = err
      c.JSON(http.StatusOK, response)
      return
  	}

  	response.Data = result
  	c.JSON(http.StatusOK, response)
  }

// Создание события
func (s *Event) CreateHandler(c *gin.Context) {
  confr, _ := c.Get(ParamConfID)
  conf := confr.(string)

  response := api.BaseResponse{}

  cs := api.CreateEvent{}
  err := c.BindJSON(&cs)
  if err != nil {
    s.log.Error(err)
    response.Error = api.ErrBindCreateEvent
    c.JSON(http.StatusBadRequest, response)
    return
  }

	model := &model.Event{
    UID: utils.RandString(16),
    TrackUID: cs.TrackUID,
    SectionUID: cs.SectionUID,
    Title: cs.Title,
    Description: cs.Description,
    StartTime: cs.StartTime,
    Duration: cs.Duration,
    Audience: cs.Audience,
    SpeakerUID: cs.SpeakersUID,
	}

  err = s.firebase.WriteEvent(conf, model)
  if err != nil {
    s.log.Error(err)
    response.Error = api.ErrWriteFirebase
    c.JSON(http.StatusBadRequest, response)
    return
  }

  err = s.db.Create(conf, model)
  if err != nil {
    response.Error = api.ErrWriteDbEvent
    c.JSON(http.StatusBadRequest, response)
    return
  }

  response.Data = model

  c.JSON(http.StatusCreated, response)
}

// Обновление события
func (s *Event) UpdateHandler(c *gin.Context) {
  confr, _ := c.Get(ParamConfID)
  conf := confr.(string)
  id := c.Param(ParamEventUID)

  response := api.BaseResponse{}

  cs := api.CreateEvent{}
  err := c.BindJSON(&cs)
  if err != nil {
    s.log.Error(err)
    response.Error = api.ErrBindCreateEvent
    c.JSON(http.StatusBadRequest, response)
    return
  }

  model := &model.Event{
    UID: id,
    TrackUID: cs.TrackUID,
    SectionUID: cs.SectionUID,
    Title: cs.Title,
    Description: cs.Description,
    StartTime: cs.StartTime,
    Duration: cs.Duration,
    Audience: cs.Audience,
    SpeakerUID: cs.SpeakersUID,
	}

  err = s.firebase.WriteEvent(conf, model)
  if err != nil {
    s.log.Error(err)
    response.Error = api.ErrWriteFirebase
    c.JSON(http.StatusBadRequest, response)
    return
  }

  err = s.db.Put(conf, model)
  if err != nil {
    response.Error = api.ErrWriteDbEvent
    c.JSON(http.StatusBadRequest, response)
    return
  }

  c.Status(http.StatusOK)
}

// Удаление события
func (s *Event) DeleteHandler(c *gin.Context) {
  confr, _ := c.Get(ParamConfID)
  conf := confr.(string)
  uid := c.Param(ParamEventUID)

  response := api.BaseResponse{}

  err := s.firebase.DeleteEvent(conf, uid)
  if err != nil {
    s.log.Error(err)
    response.Error = api.ErrWriteFirebase
    c.JSON(http.StatusBadRequest, response)
    return
  }

  err = s.db.Delete(conf, uid)
  if err != nil {
    response.Error = api.ErrWriteDbEvent
    c.JSON(http.StatusBadRequest, response)
    return
  }

  c.Status(http.StatusOK)
  }

func (s *Event) Route(r *gin.Engine, middl gin.HandlerFunc) {
  g := r.Group("events")
  g.Use(middl)
  {
    g.GET("/", s.ListHandler)
    g.GET("/:event_uid", s.GetHandler)
    g.POST("/", s.CreateHandler)
    g.PUT("/:event_uid", s.UpdateHandler)
    g.DELETE("/:event_uid", s.DeleteHandler)
  }
}
