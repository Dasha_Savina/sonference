package service

import (
  "net/http"

  "github.com/gin-gonic/gin"
  "github.com/Sirupsen/logrus"
  "bitbucket.org/confa/back/api"
  "bitbucket.org/confa/back/model"
  "bitbucket.org/confa/back/firebase"
  "bitbucket.org/confa/back/utils"
)

const (
  ParamSectionUID = "id"
  ParamConfID = "user_uid"
)

// Секция
type Section struct {
  log *logrus.Logger
  db  SectionStorier
  firebase *firebase.Firebase
}

// Создание новой Секции
func NewSection(log *logrus.Logger, db SectionStorier, firebase *firebase.Firebase) Section {
	return Section{log, db, firebase}
}

// Загрузка Секций
func (s *Section) LoadSections() error {
//  list, err := firebase
return nil
}

// Список секций
func (s *Section) ListHandler(c *gin.Context) {
  confr, _ := c.Get(ParamConfID)
  conf := confr.(string)

	response := api.BaseResponse{}

	result, err := s.db.List(conf)
	if err != nil {
		s.log.Error(err)
	}
  if result == nil {
    result = []model.Section{}
  }

	response.Data = result
	c.JSON(http.StatusOK, response)
}

// Получение секций
func (s *Section) GetHandler(c *gin.Context) {
	uid := c.Param(ParamSectionUID)
  confr, _ := c.Get(ParamConfID)
  conf := confr.(string)

	response := api.BaseResponse{}

	result, err := s.db.Get(conf, uid)
	if err != nil {
  	s.log.Error(err)
    response.Error = api.ErrWriteDbSection
    c.JSON(http.StatusOK, response)
    return
	}

	response.Data = result
	c.JSON(http.StatusOK, response)
}

//Создание секций
func (s *Section) CreateHandler(c *gin.Context) {
  confr, _ := c.Get(ParamConfID)
  conf := confr.(string)

	response := api.BaseResponse{}

  cs := api.CreateSection{}
	err := c.BindJSON(&cs)
	if err != nil {
    s.log.Error(err)
		response.Error = api.ErrBindCreateSection
		c.JSON(http.StatusBadRequest, response)
		return
	}

	model := &model.Section{
    UID: utils.RandString(16),
		Name: cs.Name,
    Color: cs.Color,
	}

  err = s.firebase.WriteSection(conf, model)
  if err != nil {
    s.log.Error(err)
		response.Error = api.ErrWriteFirebase
		c.JSON(http.StatusBadRequest, response)
		return
	}

	err = s.db.Create(conf, model)
	if err != nil {
		response.Error = api.ErrWriteDbSection
		c.JSON(http.StatusBadRequest, response)
		return
	}

  response.Data = model

	c.JSON(http.StatusCreated, response)
}

//Обновление секций
func (s *Section) UpdateHandler(c *gin.Context) {
  confr, _ := c.Get(ParamConfID)
  conf := confr.(string)
  id := c.Param(ParamSectionUID)

	response := api.BaseResponse{}

	cs := api.CreateSection{}
	err := c.BindJSON(&cs)
	if err != nil {
    s.log.Error(err)
		response.Error = api.ErrBindCreateSection
		c.JSON(http.StatusBadRequest, response)
		return
	}

  model := &model.Section{
    UID: id,
		Name: cs.Name,
    Color: cs.Color,
	}

  err = s.firebase.WriteSection(conf, model)
  if err != nil {
    s.log.Error(err)
		response.Error = api.ErrWriteFirebase
		c.JSON(http.StatusBadRequest, response)
		return
	}

	err = s.db.Put(conf, model)
	if err != nil {
		response.Error = api.ErrWriteDbSection
		c.JSON(http.StatusBadRequest, response)
		return
	}

	c.Status(http.StatusOK)
}

//Удаление секций
func (s *Section) DeleteHandler(c *gin.Context) {
  confr, _ := c.Get(ParamConfID)
  conf := confr.(string)
  uid := c.Param(ParamSectionUID)

	response := api.BaseResponse{}

  err := s.firebase.DeleteSection(conf, uid)
  if err != nil {
    s.log.Error(err)
		response.Error = api.ErrWriteFirebase
		c.JSON(http.StatusBadRequest, response)
		return
	}

	err = s.db.Delete(conf, uid)
	if err != nil {
		response.Error = api.ErrWriteDbSection
		c.JSON(http.StatusBadRequest, response)
		return
	}

	c.Status(http.StatusOK)
}

func (s *Section) Route(r *gin.Engine, middl gin.HandlerFunc) {
  g := r.Group("sections")
  g.Use(middl)
  {
    g.GET("/", s.ListHandler)
    g.GET("/:id", s.GetHandler)
    g.POST("/", s.CreateHandler)
    g.PUT("/:id", s.UpdateHandler)
    g.DELETE("/:id", s.DeleteHandler)
  }
}
