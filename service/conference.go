package service

import (
"net/http"

"gopkg.in/mgo.v2/bson"
"github.com/Sirupsen/logrus"
"github.com/gin-gonic/gin"
"bitbucket.org/confa/back/api"
"bitbucket.org/confa/back/model"
"bitbucket.org/confa/back/firebase"
)

const (
ParamConferenceUID = "conf"
)

// Конференция
type Conference struct {
  log *logrus.Logger
  firebase *firebase.Firebase
  db  StorierConference
}

// Создание новой конференции
func NewConference(log *logrus.Logger, firebase *firebase.Firebase, db StorierConference) Conference {
	return Conference{log, firebase, db}
}

// Список конференции
func (b *Conference) ConferenceListHandler(c *gin.Context) {
	response := api.BaseResponse{}

	result, err := b.db.List(bson.M{} )
	if err != nil {
		b.log.Error(err)
	}
	if result == nil {
		result = []model.Conference{}
	}
	response.Data = result
	c.JSON(http.StatusOK, response)
}

// Получение конференции
func (b *Conference) GetHandler(c *gin.Context) {
  uid := c.Param(ParamConferenceUID)

	response := api.BaseResponse{}

	result, err := b.db.Get(uid)
	if err != nil {
		b.log.Error(err)
    response.Error = api.ErrNotFound
    c.JSON(http.StatusOK, response)
    return
	}

	response.Data = result
	c.JSON(http.StatusOK, response)
}

// Создание конференции
func (b *Conference) CreateHandler(c *gin.Context) {
	response := api.BaseResponse{}

  request := &api.CreateConference{}
	err := c.BindJSON(request)
	if err != nil {
		response.Error = api.ErrBindCreateConference
		c.JSON(http.StatusBadRequest, response)
		return
	}
  user, err := b.firebase.CreateUser(request.Email, request.Password, request.Name)
  if err != nil {
    b.log.Error(err)
    response.Error = api.ErrBindCreateConference
    c.JSON(http.StatusBadRequest, response)
    return
  }

	model := &model.Conference{
		Name: request.Name,
    Email: request.Email,
    UID: user.UID,
	}
	err = b.db.Create(model)
	if err != nil {
		response.Error = api.ErrBindCreateConference
		c.JSON(http.StatusBadRequest, response)
		return
	}
  response.Data = model

  c.JSON(http.StatusCreated, response)
}

// Обновление конференции
func (b *Conference) UpdateHandler(c *gin.Context) {
  uid := c.Param(ParamConferenceUID)

  response := api.BaseResponse{}

  cf := api.UpdateConference{}
	err := c.BindJSON(&cf)
	if err != nil {
		response.Error = api.ErrWriteDbConference
		c.JSON(http.StatusBadRequest, response)
		return
	}

  model := &model.Conference{
		UID:       uid,
		Name:   cf.Name,
    Email:  cf.Email,
	}

  if _, err = b.firebase.UpdateUser(uid, cf.Email, cf.Name, cf.Password); err != nil {
    b.log.Error(err)
    response.Error = api.ErrWriteDbConference
    c.JSON(http.StatusBadRequest, response)
    return
  }

	err = b.db.Put(model)
	if err != nil {
    b.log.Error(err)
		response.Error = api.ErrWriteDbConference
		c.JSON(http.StatusBadRequest, response)
		return
	}
	c.Status(http.StatusOK)

}

// Удаление конференции
func (b *Conference) DeleteHandler(c *gin.Context) {
  uid := c.Param(ParamConferenceUID)

	response := api.BaseResponse{}

  // firebase delete
  //uid, err:=b.
	err := b.db.Delete(uid)
	if err != nil {
		response.Error = api.ErrWriteDbConference
		c.JSON(http.StatusBadRequest, response)
		return
	}
	c.Status(http.StatusOK)
}

func (b *Conference) Route(route *gin.Engine, middl gin.HandlerFunc) {
	g := route.Group("conference")
  g.Use(middl)
	{
		g.GET("/", b.ConferenceListHandler)
		g.GET("/:conf", b.GetHandler)
   	g.POST("/", b.CreateHandler)
		g.PUT("/:conf", b.UpdateHandler)
		g.DELETE("/:conf", b.DeleteHandler)
	}
}
