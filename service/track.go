package service

import (
  "net/http"

  "github.com/gin-gonic/gin"
  "github.com/Sirupsen/logrus"
  "bitbucket.org/confa/back/api"
  "bitbucket.org/confa/back/model"
  "bitbucket.org/confa/back/firebase"
  "bitbucket.org/confa/back/utils"
)

const (
  ParamTrackUID = "id"
)

// Трек
type Track struct {
  log *logrus.Logger
  db  TrackStorier
  firebase *firebase.Firebase
}

// Создание нового трека
func NewTrack(log *logrus.Logger, db TrackStorier, firebase *firebase.Firebase) Track {
	return Track{log, db, firebase}
}

// Загрузка трека
func (s *Track) LoadTrack() error {
//  list, err := firebase
return nil
}

// Список трека
func (s *Track) ListHandler(c *gin.Context) {
  confr, _ := c.Get(ParamConfID)
  conf := confr.(string)

  response := api.BaseResponse{}

  result, err := s.db.List(conf)
  if err != nil {
    s.log.Error(err)
  }
  if result == nil {
    result = []model.Track{}
  }

  response.Data = result
  c.JSON(http.StatusOK, response)
  }

// Получение трека
func (s *Track) GetHandler(c *gin.Context) {
	uid := c.Param(ParamTrackUID)
  confr, _ := c.Get(ParamConfID)
  conf := confr.(string)

	response := api.BaseResponse{}

	result, err := s.db.Get(conf, uid)
	if err != nil {
  	s.log.Error(err)
    response.Error = err
    c.JSON(http.StatusOK, response)
    return
	}

	response.Data = result
	c.JSON(http.StatusOK, response)
}

// Создание трека
func (s *Track) CreateHandler(c *gin.Context) {
  confr, _ := c.Get(ParamConfID)
  conf := confr.(string)

	response := api.BaseResponse{}

  cs := api.CreateTrack{}
	err := c.BindJSON(&cs)
	if err != nil {
    s.log.Error(err)
		response.Error = api.ErrBindCreateTrack
		c.JSON(http.StatusBadRequest, response)
		return
	}

	model := &model.Track{
    UID: utils.RandString(16),
		Name: cs.Name,
    Sort: cs.Sort,
	}

    err = s.firebase.WriteTrack(conf, model)
    if err != nil {
      s.log.Error(err)
  		response.Error = api.ErrWriteFirebase
  		c.JSON(http.StatusBadRequest, response)
  		return
  	}

  	err = s.db.Create(conf, model)
  	if err != nil {
  		response.Error = api.ErrWriteDbTrack
  		c.JSON(http.StatusBadRequest, response)
  		return
  	}

    response.Data = model

  	c.JSON(http.StatusCreated, response)
  }

// Обновление трека
func (s *Track) UpdateHandler(c *gin.Context) {
  confr, _ := c.Get(ParamConfID)
  conf := confr.(string)
  id := c.Param(ParamTrackUID)

	response := api.BaseResponse{}

	cs := api.CreateTrack{}
	err := c.BindJSON(&cs)
	if err != nil {
    s.log.Error(err)
		response.Error = api.ErrBindCreateTrack
		c.JSON(http.StatusBadRequest, response)
		return
	}

  model := &model.Track{
    UID: id,
		Name: cs.Name,
    Sort: cs.Sort,
	}

  err = s.firebase.WriteTrack(conf, model)
  if err != nil {
    s.log.Error(err)
    response.Error = api.ErrWriteFirebase
    c.JSON(http.StatusBadRequest, response)
    return
  }

  err = s.db.Put(conf, model)
  if err != nil {
    response.Error = api.ErrWriteDbTrack
    c.JSON(http.StatusBadRequest, response)
    return
  }

  c.Status(http.StatusOK)
}

// Удаление трека
func (s *Track) DeleteHandler(c *gin.Context) {
  confr, _ := c.Get(ParamConfID)
  conf := confr.(string)
  uid := c.Param(ParamTrackUID)

  response := api.BaseResponse{}

  err := s.firebase.DeleteTrack(conf, uid)
  if err != nil {
    s.log.Error(err)
    response.Error = api.ErrWriteFirebase
    c.JSON(http.StatusBadRequest, response)
    return
  }

  err = s.db.Delete(conf, uid)
  if err != nil {
    response.Error = api.ErrWriteDbTrack
    c.JSON(http.StatusBadRequest, response)
    return
  }

  c.Status(http.StatusOK)
  }


func (s *Track) Route(r *gin.Engine, middl gin.HandlerFunc) {
  g := r.Group("tracks")
  g.Use(middl)
  {
    g.GET("/", s.ListHandler)
    g.GET("/:id", s.GetHandler)
    g.POST("/", s.CreateHandler)
    g.PUT("/:id", s.UpdateHandler)
    g.DELETE("/:id", s.DeleteHandler)
  }
}
