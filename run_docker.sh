#/bin/bash

docker run -d \
  --name n1.service-starter \
  --restart always \
  --net=starternet \
  --publish 3001:8080 \
  -e DB_HOST=mongodb \
  -e DB_USER=admin \
  -e DB_PASS=dbpassword \
  -e DB_NAME=StarterServiceDb \
  -e SERVICE_NAME=service-starter \
  -e SERVICE_TAGS=starter \
  -e SERVICE_CHECK_INTERVAL=60s \
  -e SERVICE_CHECK_TIMEOUT=1s \
  -e SERVICE_HOST=:8080 \
  alezhka/touch-server