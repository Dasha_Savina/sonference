default: build

clean:
	rm -f cmd/server/server

build:
	cd cmd/server; \
	go build

deps:
	cd cmd/server; \
	go get

test:
	go test ./...