# back

Бэкенд сервера конференций

# Golang
https://gobyexample.com

# Gin(HTTP web framework)
https://github.com/gin-gonic/gin

# Log
https://github.com/Sirupsen/logrus

# Validators
https://github.com/asaskevich/govalidator/tree/v9

# Mongodb client
https://github.com/go-mgo/mgo/tree/v2

# Firebase
https://github.com/firebase/firebase-admin-go