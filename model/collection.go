package model

// Структура коллекции
type Collection struct {
 Count int          `json:"count"`
 Items interface{}  `json:"items"`
}

// Создание новой коллекции
func NewCollection(count int, items interface{}) Collection {
  return Collection{count,items}
}
