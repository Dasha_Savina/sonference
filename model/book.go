package model

import (
	"gopkg.in/mgo.v2/bson"
)

// Book.
type Book struct {
	ID 		bson.ObjectId 	`bson:"_id,omitempty" json:"id"`
	Name    string 			`bson:"email" json:"name"`
}