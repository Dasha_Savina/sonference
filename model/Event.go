package model

type Event struct {
	UID string             	`json:"uid"`
	TrackUID string         `json:"track_uid"`
	SectionUID string       `json:"section_uid"`
	Title string          	`json:"title"`
	Description string    	`json:"description"`
	StartTime int64       	`json:"start_time"`
	Duration int          	`json:"duration"`
	Audience string       	`json:"audience"`
	SpeakerUID []string     `json:"speakers"`
}

type FullEvent struct {
	UID string           `json:"uid"`
	Track Track          `json:"track"`
	Section Section      `json:"section"`
	Title string         `json:"title"`
	Description string   `json:"description"`
	StartTime int64      `json:"start_time"`
	Duration int         `json:"duration"`
	Audience string      `json:"audience"`
	Speakers []Speaker   `json:"speakers"`
}

type Section struct {
	UID string        `json:"uid"`
	Name string       `json:"name"`
	Color string      `json:"color"`
}

type Speaker struct {
	UID string         `json:"uid"`
	Name string        `json:"name"`
	Work string        `json:"work"`
	Info string        `json:"info"`
	Photo string       `json:"photo"`
}

type Track struct {
	UID  string		`json:"uid"`
	Name string		`json:"name"`
	Sort string		`json:"sort"`
}

type Rating struct {
	UID 	 		 string	 	`json:"uid"`
  Rating     int		`json:"rating"`
	Comment    string		`json:"comment"`
}
