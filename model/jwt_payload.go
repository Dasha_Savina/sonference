package model

// pole uid firebase
//
type JWTPayload struct{
  UserUID	      string     	`json:"user_uid"`
  Role          string      `json:"role"`
}
