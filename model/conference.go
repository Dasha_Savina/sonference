package model
import (
  //"time"
)

// Структура конференции
type Conference struct{
  UID	      string     	`json:"uid"`
	Name      string 			`json:"name"`
  Email     string      `json:"email"`
  
  //CreateAt  time.Time   `bson:"create_at" json:"create_at"`
//  EditAt    *time.Time  `bson:"edit_at" json:"edit_at"`
  //DeleteAt  *time.Time  `bson:"delete_at" json:"delete_at"`
}
