FROM golang:1.6.3-alpine
MAINTAINER alezhk@gmail.com

COPY . /go/src/bitbucket.org/confa/back/

RUN apk add --no-cache git

WORKDIR /go/src/bitbucket.org/confa/back/

RUN go get github.com/Masterminds/glide
RUN glide up

CMD go run cmd/server/server.go server

EXPOSE 8080