package api

import (
	"strconv"

	"gopkg.in/asaskevich/govalidator.v4"
)

const (
	DefaultSkip  = 0
	DefaultLimit = 50
)

type ListRequest struct {
	Skip  int
	Limit int
}

type GetRequest struct {
	BookID string
}
type GetRequestConference struct {
	ConferenceID string
}


type DeleteRequest struct {
	BookID string
}
type DeleteRequestConference struct {
	ConferenceID string
}

type CreateBook struct {
	Name string `json:"name"`
}
type CreateConference struct {
	Name string `json:"name"`
}

type UpdateBook struct {
	ID 		string `json:"id"`
	Name 	string `json:"name"`
}

type UpdateConference struct {
	ID 		string `json:"id"`
	Name 	string `json:"name"`
}
func NewGetRequest(id string) GetRequest {
	return GetRequest{id}
}

func NewDeleteRequest(id string) DeleteRequest {
	return DeleteRequest{id}
}

func NewListRequest(skip, limit string) ListRequest {
	s := DefaultSkip
	l := DefaultLimit

	if !govalidator.IsNull(skip) {
		if i, err := strconv.Atoi(skip); err == nil {
			s = i
		}
	}

	if !govalidator.IsNull(limit) {
		if i, err := strconv.Atoi(limit); err == nil {
			l = i
		}
	}

	if s <= 0 {
		s = DefaultSkip
	}

	if l <= 0 {
		l = DefaultLimit
	}

	return ListRequest{s, l}
}
