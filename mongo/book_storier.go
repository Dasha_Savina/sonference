package mongo

import (
	"bitbucket.org/confa/back/model"
	mgo "gopkg.in/mgo.v2"
)

type BookStorier struct {
	session  *mgo.Session
	database string
}

func NewBookStorier(session *mgo.Session, database string) (s BookStorier, e error) {
	s.database = database
	s.session = session
	return
}

func (s BookStorier) Create(value *model.Book) error {
	return nil
}

// Список комнат.
func (s BookStorier) List(match interface{}, skip, limit int) ([]model.Book, error) {
	var ri []model.Book
	return ri, nil
}

// Получение комнаты.
func (s BookStorier) Get(key string) (*model.Book, error) {
	return nil, nil
}

// Обновление комнаты.
func (s BookStorier) Put(value *model.Book) error {
	return nil
}
