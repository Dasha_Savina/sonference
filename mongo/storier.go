package mongo

import (
	"errors"

	mgo "gopkg.in/mgo.v2"
)

// DbStorier stores database connection information.
type DbStorier struct {
	mongoURL string
	database string
	session  *mgo.Session
	BookStorier
}

func mkmgoerror(msg string) error {
	return errors.New("mongobackend: " + msg)
}

// NewMongoStorier initializes a new backend.
// Be sure to call Close() on this to clean up the mongodb connection.
// Example:
//     backend = httpauth.MongodbAuth("mongodb://127.0.0.1/", "auth")
//     defer backend.Close()
func NewMongoStorier(mongoURL string, database string) (b DbStorier, e error) {
	// Set up connection to database
	b.mongoURL = mongoURL
	b.database = database
	session, err := mgo.Dial(b.mongoURL)
	if err != nil {
		return b, mkmgoerror(err.Error())
	}
	err = session.Ping()
	if err != nil {
		return b, mkmgoerror(err.Error())
	}

	// Ensure that the email field is unique
	indexUsersEmail := mgo.Index{
		Key:    []string{"email"},
		Unique: true,
	}

	err = session.DB(b.database).C("users").EnsureIndex(indexUsersEmail)
	if err != nil {
		return b, mkmgoerror(err.Error())
	}
	b.session = session

	rs, err := NewBookStorier(session, database)
	if err != nil {
		return b, err
	}
	b.BookStorier = rs
	return
}

// Close cleans up the backend once done with. This should be called before
// program exit.
func (s *DbStorier) Close() {
	if s.session != nil {
		s.session.Close()
	}
}
