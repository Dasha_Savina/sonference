package utils

import (
	"math/rand"
	"time"
)

func SeedReset() {
	rand.Seed(time.Now().UTC().UnixNano())
}

func RandString(n int) string {
	const alphanum = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
	var bytes = make([]byte, n)
	rand.Read(bytes)
	for i, b := range bytes {
		bytes[i] = alphanum[b%byte(len(alphanum))]
	}
	return string(bytes)
}

func RandInt(min, max int) int {
	return min + rand.Intn(max-min)
}
