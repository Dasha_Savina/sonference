package api

import (
	"strconv"

	"gopkg.in/asaskevich/govalidator.v4"
)

const (
	DefaultSkip  = 0
	DefaultLimit = 50
)

type ListRequest struct {
	Skip  int
	Limit int
}

type GetRequest struct {
	BookID string
}
type GetRequestConference struct {
	ConferenceUID string
}


type DeleteRequest struct {
	BookID string
}
type DeleteRequestConference struct {
	ConferenceUID string
}

type CreateBook struct {
	Name string `json:"name"`
	Email string `json:"email"`
	Password string `json:"password"`
}
type CreateConference struct {
	Name string `json:"name"`
	Email string `json:"email"`
	Password string `json:"password"`
}

type UpdateBook struct {
	ID 		string `json:"id"`
	Name 	string `json:"name"`
}

type UpdateConference struct {
	Name 	string `json:"name"`
	Email string `json:"email"`
	Password string `json:"password"`
}

type CreateSection struct {
	Name string       `json:"name"`
	Color string      `json:"color"`
}

type CreateSpeaker struct {
	Name string        `json:"name"`
	Work string        `json:"work"`
	Info string        `json:"info"`
	Photo string       `json:"photo"`
}

type CreateTrack struct {
	Name string		`json:"name"`
	Sort string		`json:"sort"`
}

type CreateRating struct {
	EventUID string `json:"event_uid"`
	Rating int			`json:"rating"`
	Comment string		`json:"comment"`
}

type CreateEvent struct {
	TrackUID string         `json:"track_uid"`
	SectionUID string       `json:"section_uid"`
	Title string          	`json:"title"`
	Description string    	`json:"description"`
	StartTime int64       	`json:"start_time"`
	Duration int          	`json:"duration"`
	Audience string       	`json:"audience"`
	SpeakersUID []string     `json:"speakers"`
}

type SignIn struct {
	Login string 		`json:"login"`
	Password string `json:"password"`
}

func NewGetRequest(id string) GetRequest {
	return GetRequest{id}
}

func NewDeleteRequest(id string) DeleteRequest {
	return DeleteRequest{id}
}

func NewListRequest(skip, limit string) ListRequest {
	s := DefaultSkip
	l := DefaultLimit

	if !govalidator.IsNull(skip) {
		if i, err := strconv.Atoi(skip); err == nil {
			s = i
		}
	}

	if !govalidator.IsNull(limit) {
		if i, err := strconv.Atoi(limit); err == nil {
			l = i
		}
	}

	if s <= 0 {
		s = DefaultSkip
	}

	if l <= 0 {
		l = DefaultLimit
	}

	return ListRequest{s, l}
}
