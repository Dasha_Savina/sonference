package api

import "fmt"

var (
	// Auth

	CodeBindCreateBook 	= 100
	CodeWriteDbBook     = 101
	CodeNotFound 		= 200
	CodeCreatConference = 201
	CodeErrWriteDbConference =202
	CodeErrLoadUsers =200
	CodeWriteDbSection = 203
	CodeWriteFirebase = 204
	CodeBindCreateSection = 205
	CodeBindCreateEvent = 206
	CodeBindCreateTrack = 207
	CodeErrWriteDbSpeaker = 208
	CodeErrBindCreateSpeaker = 209
	CodeErrBindCreateRating = 210
	CodeErrWriteDbRating = 211
	CodeBindSignIn = 212

	ErrBindCreateBook		= NewError(CodeBindCreateBook, "Cannot bind create book.")
	ErrWriteDbBook			= NewError(CodeWriteDbBook, "Error write db.")
	ErrNotFound             = NewError(CodeNotFound, "Not found")
	ErrLoadUsers =NewError(CodeErrLoadUsers, "Cannot load users")
	ErrBindCreateConference = NewError(CodeCreatConference, "Cannot create a conference")
	ErrWriteDbConference = NewError(CodeErrWriteDbConference, "Cannot write a conference")
	ErrWriteDbSection = NewError(CodeWriteDbSection, "Cannot write a section")
	ErrWriteDbTrack = NewError(CodeWriteDbSection, "Cannot write a track")
	ErrWriteDbEvent = NewError(CodeWriteDbSection, "Cannot write a event")
	ErrWriteDbSpeackers = NewError(CodeWriteDbSection, "Cannot write a speakers")
	ErrWriteFirebase = NewError(CodeWriteFirebase, "Error write firebase")
	ErrBindCreateSection = NewError(CodeBindCreateSection, "Error bind section")
	ErrResponseEvent = NewError(CodeBindCreateEvent, "Error response event")
	ErrBindCreateEvent = NewError(CodeBindCreateEvent, "Error bind event")
  ErrBindCreateTrack = NewError(CodeBindCreateTrack, "Error bind track")
	ErrWriteDbSpeaker = NewError(CodeErrWriteDbSpeaker, "Cannot write a speaker")
	ErrBindCreateSpeaker = NewError(CodeErrBindCreateSpeaker, "Error bind speaker")
	ErrBindCreateRating = NewError(CodeErrBindCreateRating, "Error bind rating")
	ErrWriteDbRating = NewError(CodeErrWriteDbRating, "Cannot write a rating" )
	ErrBindSignIn = NewError(CodeBindSignIn, "Error bind sign in")
)

//Error api response class, provide error code and message.
type Error struct {
	Message string `json:"message"`
	Code    int    `json:"code"`
}

//NewError constructor return new Error instance.
func NewError(code int, msg string) *Error {
	return &Error{
		Message: msg,
		Code:    code,
	}
}

func (e Error) Error() string {
	return fmt.Sprintf("%d - %s", e.Code, e.Message)
}
