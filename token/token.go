package token

import (
    "fmt"
    "net/http"
    "strings"
    "errors"

    "github.com/Sirupsen/logrus"
  	"github.com/gin-gonic/gin"
    "github.com/dgrijalva/jwt-go"
    "bitbucket.org/confa/back/model"
)

const (
  RoleAdmin = "admin"
  RoleUser = "user"
)

type JWT struct{
  log *logrus.Logger
  secret string
}

func NewJWT(log *logrus.Logger, secret string) JWT {
    return JWT{log, secret}
}

func (j *JWT) Authentificate(roles ...string) gin.HandlerFunc {
	return func(c *gin.Context) {
    raw, err := parseToken(c)
		if err != nil {
			respondWithError(c, http.StatusUnauthorized, err.Error())
			return
		}

    j.log.Printf("raw: %s", raw)

    token, err := j.Parse(raw)
  	if err != nil {
        j.log.Error(err)
      	respondWithError(c, http.StatusUnauthorized, err.Error())
        return
  	}

    can := false
    for _, v := range roles {
      if token.Role == v {
        can = true
        break
      }
    }

    if !can {
      j.log.Error(err)
      respondWithError(c, http.StatusUnauthorized, err.Error())
      return
    }

    j.log.Printf("raw: %+v", token)

		c.Set("user_uid", token.UserUID)
		c.Next()
	}
}


func parseToken(c *gin.Context) (string, error) {
	authHeader := c.Request.Header.Get("Authorization")
	if authHeader == "" {
		return "", errors.New("Auth header empty")
	}
	parts := strings.SplitN(authHeader, " ", 2)
	if !(len(parts) == 2 && parts[0] == "Bearer") {
		return "", errors.New("Invalid auth header")
	}
	return parts[1], nil
}

func respondWithError(c *gin.Context, code int, message string) {
	c.JSON(http.StatusUnauthorized, gin.H{
		"code":    http.StatusUnauthorized,
		"message": message,
	})
	c.Abort()
}

func (j *JWT) Generate(userUID string, role string) (string, error) {
  token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
    "user_uid" : userUID,
    "role": role,
  })
  return token.SignedString([]byte(j.secret))
}

func (j *JWT) Parse(raw string) (*model.JWTPayload, error) {
  token, err := jwt.Parse(raw, func(token *jwt.Token) (interface{}, error) {
    if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
       return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
   }
   return []byte(j.secret), nil
  })

  if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
    payload := &model.JWTPayload {
      UserUID: claims["user_uid"].(string),
      Role: claims["role"].(string),
    }
    return payload, nil
  }

  if ve, ok := err.(*jwt.ValidationError); ok {
      if ve.Errors&jwt.ValidationErrorMalformed != 0 {
          return nil, fmt.Errorf("That's not even a token")
      } else if ve.Errors&(jwt.ValidationErrorExpired|jwt.ValidationErrorNotValidYet) != 0 {
          return nil, fmt.Errorf("Timing is everything")
      }
      return nil, fmt.Errorf("Couldn't handle this token:", err)
  }

  return nil, fmt.Errorf("Couldn't handle this token:", err)
}
