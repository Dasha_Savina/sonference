package main

import (
	"os"

	"github.com/Sirupsen/logrus"

	"bitbucket.org/confa/back/service"
	"github.com/codegangsta/cli"
)

const version = "0.0.1"

func main() {
	log := setupLogger()
	app := cli.NewApp()
	app.Name = "Service starter"
	app.Version = version
	app.Commands = []cli.Command{
		{
			Name:  "server",
			Usage: "Service starter",
			Action: func(c *cli.Context) error {
				svc := service.NewAppService(version, log)
				if err := svc.Run(); err != nil {
					log.Fatal(err)
					return err
				}
				return nil
			},
		},
	}
	app.Run(os.Args)
}

// Create a new instance of the logger. You can have any number of instances.
func setupLogger() *logrus.Logger {
	// Create a new instance of the logger. You can have any number of instances.
	log := logrus.New()
	// Output to stderr instead of stdout, could also be a file.
	log.Out = os.Stderr

	// Log as JSON instead of the default ASCII formatter.
	log.Formatter = &logrus.TextFormatter{}

	// Only log the warning severity or above.
	log.Level = logrus.DebugLevel
	return log
}
