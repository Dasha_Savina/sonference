package firebase

import (
  "os"
  "fmt"
  //"path/filepath"
  "golang.org/x/net/context"

  "github.com/Sirupsen/logrus"
	fb "firebase.google.com/go"
  "firebase.google.com/go/auth"
	"firebase.google.com/go/db"

  "google.golang.org/api/option"

)

type Firebase struct {
  ctx context.Context
  log *logrus.Logger
  app *fb.App
  auth *auth.Client
  db *db.Client
  rootPathDB string
}

func NewFirebase(log *logrus.Logger, key string) (*Firebase, error) {
  ctx := context.Background()
	conf := &fb.Config{
		DatabaseURL: "https://testproject-b5cf1.firebaseio.com",
	}

  dir, err := os.Getwd()
  if err!= nil {
    return nil, err
  }
  fileName := fmt.Sprintf("%s/keys/%s", dir, key)
  log.Println(fileName)

  opt := option.WithCredentialsFile(fileName)
  app, err := fb.NewApp(ctx, conf, opt)
  if err!= nil{
    return nil, err
  }

  // Инциализация БД.
  db, err := app.Database(ctx)
  if err != nil {
    return nil, err
  }

  rootPathDB := "restricted_access/secret_document/"
  ref := db.NewRef(rootPathDB)
	var data map[string]interface{}
	if err := ref.Get(ctx, &data); err != nil {
		log.Fatalln("Error reading from database:", err)
	}
	fmt.Println(data)

  // Инциализация и авторизация
  auth, err := app.Auth(ctx)
  if err != nil {
    return nil, err
  }

  return &Firebase{ctx, log, app, auth, db, rootPathDB}, err
}
