package firebase

import (
  "fmt"

  "bitbucket.org/confa/back/model"
)

func (f *Firebase) WriteEvent(userID string, s *model.Event) error {
  path := fmt.Sprintf("%s/%s/events/%s", f.rootPathDB, userID, s.UID)
	return f.writeDb(path, s)
}

func (f *Firebase) DeleteEvent(userID, UID string) error {
  path := fmt.Sprintf("%s/%s/events/%s", f.rootPathDB, userID, UID)
	return f.writeDb(path, nil)
}

func (f *Firebase) ReadEvent(userID, UID string) (model.Event, error) {
  path := fmt.Sprintf("%s/%s/events/%s", f.rootPathDB, userID, UID)
  var model model.Event
  err := f.readDb(path, &model)
  return model, err
}

func (f *Firebase) ListEvent(userID string) ([]model.Event, error) {
  path := fmt.Sprintf("%s/%s/events", f.rootPathDB, userID)
  var list map[string]*model.Event
  var result []model.Event
  err := f.readDb(path, &list)
  for _, v := range list {
    if v != nil {
      result = append(result, *v)
    }
  }
  return result, err
}
