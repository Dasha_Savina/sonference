package firebase

import (
  "fmt"

  "bitbucket.org/confa/back/model"
)

func (f *Firebase) WriteFullEvent(userID string, s *model.FullEvent) error {
  path := fmt.Sprintf("%s/%s/fullevents/%s", f.rootPathDB, userID, s.UID)
	return f.writeDb(path, s)
}

func (f *Firebase) DeleteFullEvent(userID, UID string) error {
  path := fmt.Sprintf("%s/%s/fullevents/%s", f.rootPathDB, userID, UID)
	return f.writeDb(path, nil)
}

func (f *Firebase) ReadFullEvent(userID, UID string) (model.FullEvent, error) {
  path := fmt.Sprintf("%s/%s/fullevents/%s", f.rootPathDB, userID, UID)
  var model model.FullEvent
  err := f.readDb(path, &model)
  return model, err
}

func (f *Firebase) ListFullEvent(userID string) ([]model.FullEvent, error) {
  path := fmt.Sprintf("%s/%s/fullevents", f.rootPathDB, userID)
  var list map[string]*model.FullEvent
  var result []model.FullEvent
  err := f.readDb(path, &list)
  for _, v := range list {
    if v != nil {
      result = append(result, *v)
    }
  }
  return result, err
}
