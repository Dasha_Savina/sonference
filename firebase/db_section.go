package firebase

import (
  "fmt"

  "bitbucket.org/confa/back/model"
)

func (f *Firebase) WriteSection(userID string, s *model.Section) error {
  path := fmt.Sprintf("%s/%s/sections/%s", f.rootPathDB, userID, s.UID)
	return f.writeDb(path, s)
}

func (f *Firebase) DeleteSection(userID, UID string) error {
  path := fmt.Sprintf("%s/%s/sections/%s", f.rootPathDB, userID, UID)
	return f.writeDb(path, nil)
}

func (f *Firebase) ReadSection(userID, UID string) (model.Section, error) {
  path := fmt.Sprintf("%s/%s/sections/%s", f.rootPathDB, userID, UID)
  var model model.Section
  err := f.readDb(path, &model)
  return model, err
}

func (f *Firebase) ListSection(userID string) ([]model.Section, error) {
  path := fmt.Sprintf("%s/%s/sections", f.rootPathDB, userID)
  var list map[string]*model.Section
  var result []model.Section
  err := f.readDb(path, &list)
  for _, v := range list {
    if v != nil {
      result = append(result, *v)
    }
  }
  return result, err
}
