package firebase

import (
  "fmt"

  "bitbucket.org/confa/back/model"
)

func (f *Firebase) WriteSpeaker(userID string, s *model.Speaker) error {
  path := fmt.Sprintf("%s/%s/speakers/%s", f.rootPathDB, userID, s.UID)
	return f.writeDb(path, s)
}

func (f *Firebase) DeleteSpeaker(userID, UID string) error {
  path := fmt.Sprintf("%s/%s/speakers/%s", f.rootPathDB, userID, UID)
	return f.writeDb(path, nil)
}

func (f *Firebase) ReadSpeaker(userID, UID string) (model.Speaker, error) {
  path := fmt.Sprintf("%s/%s/speakers/%s", f.rootPathDB, userID, UID)
  var model model.Speaker
  err := f.readDb(path, &model)
  return model, err
}

func (f *Firebase) ListSpeaker(userID string) ([]model.Speaker, error) {
  path := fmt.Sprintf("%s/%s/speakers", f.rootPathDB, userID)
  var list map[string]*model.Speaker
  var result []model.Speaker
  err := f.readDb(path, &list)
  for _, v := range list {
    if v != nil {
      result = append(result, *v)
    }
  }
  return result, err
}
