package firebase

import (
  "fmt"

  "bitbucket.org/confa/back/model"
)

func (f *Firebase) WriteTrack(userID string, s *model.Track) error {
  path := fmt.Sprintf("%s/%s/tracks/%s", f.rootPathDB, userID, s.UID)
	return f.writeDb(path, s)
}

func (f *Firebase) DeleteTrack(userID, UID string) error {
  path := fmt.Sprintf("%s/%s/tracks/%s", f.rootPathDB, userID, UID)
	return f.writeDb(path, nil)
}

func (f *Firebase) ReadTrack(userID, UID string) (model.Track, error) {
  path := fmt.Sprintf("%s/%s/tracks/%s", f.rootPathDB, userID, UID)
  var model model.Track
  err := f.readDb(path, &model)
  return model, err
}

func (f *Firebase) ListTrack(userID string) ([]model.Track, error) {
  path := fmt.Sprintf("%s/%s/tracks", f.rootPathDB, userID)
  var list map[string]*model.Track
  var result []model.Track
  err := f.readDb(path, &list)
  for _, v := range list {
    if v != nil {
      result = append(result, *v)
    }
  }
  return result, err
}
