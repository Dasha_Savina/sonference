package firebase

import (
  "fmt"

  "bitbucket.org/confa/back/model"
)

func (f *Firebase) WriteRating(userUID string, eventUID string, s *model.Rating) error {
  path := fmt.Sprintf("%s/%s/ratings/%s/%s", f.rootPathDB, userUID, eventUID, s.UID)
	return f.writeDb(path, s)
}

func (f *Firebase) DeleteRating(userID, eventUID, UID string) error {
  path := fmt.Sprintf("%s/%s/ratings/%s/%s", f.rootPathDB, userID, eventUID, UID)
	return f.writeDb(path, nil)
}

func (f *Firebase) ReadRating(userID, eventUID, UID string) (model.Rating, error) {
  path := fmt.Sprintf("%s/%s/ratings/%s/%s", f.rootPathDB, userID, eventUID, UID)
  var model model.Rating
  err := f.readDb(path, &model)
  return model, err
}

func (f *Firebase) ListRating(userID, eventUID string) ([]model.Rating, error) {
  path := fmt.Sprintf("%s/%s/ratings/%s", f.rootPathDB, userID, eventUID)
  var list map[string]*model.Rating
  var result []model.Rating
  err := f.readDb(path, &list)
  for _, v := range list {
    if v != nil {
      result = append(result, *v)
    }
  }
  return result, err
}
