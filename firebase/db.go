package firebase

import (
  //"path/filepath"
)

func (f *Firebase) writeDb(path string, model interface{}) error {
  ref := f.db.NewRef(path)
	return ref.Set(f.ctx, model)
}

func (f *Firebase) readDb(path string, model interface{}) error {
      ref := f.db.NewRef(path)
    	return ref.Get(f.ctx, &model)
  	}

func (f *Firebase) TestDB() {
  ref := f.db.NewRef(f.rootPathDB)
  usersRef := ref.Child("users")
	err := usersRef.Set(f.ctx, map[string]string{
		"alanisawesome": "hfghfg",
		"gracehop":" gbhfg",
	})
	if err != nil {
		f.log.Fatalln("Error setting value:", err)
	}
  f.log.Println("TestDb")
}
