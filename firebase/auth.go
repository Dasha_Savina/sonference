package firebase

import (
  "encoding/base64"

  //"path/filepath"
  "firebase.google.com/go/auth"

  "google.golang.org/api/iterator"
)

func (f *Firebase) GetUserByEmail(email string) (*auth.UserRecord, error) {
  return f.auth.GetUserByEmail(f.ctx, email)
}

func (f *Firebase) GetToken(email, password string) (string, error) {
  token := ""
  user, err := f.auth.GetUserByEmail(f.ctx, email)
  if err != nil {
    return token, err
  }

  ph := base64.RawURLEncoding.EncodeToString([]byte(password))
  if user.CustomClaims["pass_hash"].(string) != ph {
    return token, err
  }

  return f.auth.CustomToken(f.ctx, user.UID)
}

func (f *Firebase) RevokeToken(uid string) error {
  return f.auth.RevokeRefreshTokens(f.ctx, uid)
}

func (f *Firebase) CreateUser(email, password, name string) (*auth.UserRecord, error) {
  params := (&auth.UserToCreate{}).
		Email(email).
		EmailVerified(false).
		Password(password).
		DisplayName(name).
		Disabled(false)
	  user, err := f.auth.CreateUser(f.ctx, params)
    if err != nil {
      return nil, err
    }

    claims := map[string]interface{} {"pass_hash":  base64.RawURLEncoding.EncodeToString([]byte(password))}
    _ = f.auth.SetCustomUserClaims(f.ctx, user.UID, claims)
    return f.GetUser(user.UID)
}

func (f *Firebase) GetUser(uid string) (*auth.UserRecord, error) {
  return f.auth.GetUser(f.ctx, uid)
}

func (f *Firebase) UpdateUser(uid string, email, password, name string) (*auth.UserRecord, error) {
  params :=(&auth.UserToUpdate{}).
  		Email(email).
  		DisplayName(name).
  		Disabled(false)

  if password != "" {
    params.Password(password)
    claims := map[string]interface{} {"pass_hash":  base64.RawURLEncoding.EncodeToString([]byte(password))}
    _ = f.auth.SetCustomUserClaims(f.ctx, uid, claims)
  }

  return f.auth.UpdateUser(f.ctx, uid, params)
}

func (f *Firebase) DeleteUser(uid string) error {
  return f.auth.DeleteUser(f.ctx, uid)
}

func (f *Firebase) ListUsers() ([]auth.UserRecord, error) {
  iter := f.auth.Users(f.ctx, "")
  var users []auth.UserRecord
  for {
  		user, err := iter.Next()
  		if err == iterator.Done {
  			return users, nil
  		}
  		if err != nil {
  			f.log.Fatalf("error listing users: %s\n", err)
  		}
      users = append(users, *user.UserRecord)
    }
  return users, nil
}
